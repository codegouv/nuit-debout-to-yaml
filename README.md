# nuit-debout-to-yaml

Convert Nuit Debout catalog of services to a Git repository of YAML files.


## Usage

```bash
./nuit_debout_to_yaml.py yaml/
```
